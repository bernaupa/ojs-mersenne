<?php

/**
 * @file tools/upgradeSourceGalleys.php
 *
 * @class upgradeSourceGalleys
 * @ingroup tools
 *
 * @brief CLI tool for turning production-ready article archive into source galleys.
 */

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/tools/bootstrap.inc.php');

class upgradedSourceGalleys extends CommandLineTool {

	/** @var $journalAcro string */
	var $journalAcronym;

	/**
	 * Constructor.
	 * @param $argv array command-line arguments
	 */
	function __construct($argv = array()) {
		parent::__construct($argv);

		if (!isset($this->argv[0])) {
			$this->usage();
			exit(1);
		}

		$this->journalAcronym = $this->argv[0];
	}

	/**
	 * Print command usage information.
	 */
	function usage() {
		echo "Mersenne upgrade tool for migrating article archives as galleys\n"
			. "Use this tool to upgrade article archives.\n\n"
			. "Usage: {$this->scriptName} journalAcronym\n"
			. "journalAcronym    The acronym of the journal.\n";
	}

	/**
	 * Execute the merge users command.
	 */
	function execute() {
		$journalDao = DAORegistry::getDAO('JournalDAO');
		$journals = $journalDao->getBySetting('acronym', $this->journalAcronym);
		if ($journals->getCount() == 0) {
			die('Unknown journal journal ' . $this->journalAcronym);
		}
		assert($journals->getCount() == 1);
		$journal = $journals->next();
		$contextId = $journal->getId();

		import('classes.core.ServicesContainer');
		$submissionService = ServicesContainer::instance()->get('submission');

		$params = array (
			'stageIds' => [ WORKFLOW_STAGE_ID_PRODUCTION, WORKFLOW_STAGE_ID_PUBLISHED ],
			'count'    => -1,
			'offset'   => -1
		);
		$submissions = $submissionService->getSubmissions($contextId, $params);
		foreach ($submissions as $submission) {
			$this->upgradeArticleArchive($journal, $submission);
		}
	}

	private function upgradeArticleArchive($journal, $article) {
		import('lib.pkp.classes.submission.SubmissionFile'); // import constants
		import('plugins.generic.mersenne.MersennePlugin'); // import constants

		$articleId = $article->getId();

		$submissionFileDao = DAORegistry::getDAO('SubmissionFileDAO');
		$submissionFiles = $submissionFileDao->getLatestRevisions($articleId, SUBMISSION_FILE_PRODUCTION_READY);
		$sourceFile = NULL;
		foreach ($submissionFiles as $file) {
			if ($file->getDocumentType() != DOCUMENT_TYPE_ZIP) continue;

			if (isset($sourceFile)) {
				fatalError("Duplicate article archive in production ready files of submission #" . $article->getId());
			}
			$sourceFile = $file;
		}

		if (empty($sourceFile)) {
			// no article archive found
			return;
		}

		$sourceFile->setFileStage(SUBMISSION_FILE_PROOF);

		$articleGalleyDao = DAORegistry::getDAO('ArticleGalleyDAO');
		// search for an existing source galley
		$articleGalley = array_shift(
			array_filter(
				$articleGalleyDao->getBySubmissionId($articleId)->toArray(),
				function ($aG) { return $aG->getLabel() === MERSENNE_ARTICLE_SOURCE_GALLEY; }
			));

		if (!$articleGalley) {
			// no source galley, create new one
			$articleGalley = $articleGalleyDao->newDataObject();
			$articleGalley->setSubmissionId($articleId);
			$articleGalley->setLabel(MERSENNE_ARTICLE_SOURCE_GALLEY);
			$articleGalleyDao->insertObject($articleGalley);
		}
		$sourceFile->setAssocType(ASSOC_TYPE_REPRESENTATION);
		$sourceFile->setAssocId($articleGalley->getId());

		$submissionFileDao->updateObject($sourceFile);

		$articleGalley->setFileId($sourceFile->getFileId());
		$articleGalleyDao->updateObject($articleGalley);

		printf("%s -> %s\n", $articleId, $sourceFile->getFilePath());
	}
}

$tool = new upgradedSourceGalleys(isset($argv) ? $argv : array());
$tool->execute();
?>