<?php

/**
 * @file tools/assignMersenneDOIs.php
 *
 * @class assignMersenneDOIs
 * @ingroup tools
 *
 * @brief CLI tool for assigning Mersenne DOIs to already accepted submissions.
 */

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/tools/bootstrap.inc.php');

class assignMersenneDOIs extends CommandLineTool {

	/** @var $journalAcro string */
	var $journalAcronym;

	/**
	 * Constructor.
	 * @param $argv array command-line arguments
	 */
	function __construct($argv = array()) {
		parent::__construct($argv);

		if (!isset($this->argv[0])) {
			$this->usage();
			exit(1);
		}

		$this->journalAcronym = $this->argv[0];
	}
	
	/**
	 * Print command usage information.
	 */
	function usage() {
		echo "Mersenne DOI assignement tool\n"
			. "Use this tool to assign new Mersenne DOIs to the accepted submissions of a journal.\n\n"
			. "Usage: {$this->scriptName} journalAcronym\n"
			. "journalAcronym    The acronym of the journal.\n";
	}

	/**
	 * Execute the merge users command.
	 */
	function execute() {
		$journalDao = DAORegistry::getDAO('JournalDAO');
		$journals = $journalDao->getBySetting('acronym', $this->journalAcronym);
		if ($journals->getCount() == 0) {
			die('Unknown journal journal ' . $this->journalAcronym);
		}
		assert($journals->getCount() == 1);
		$journal = $journals->next();
		$contextId = $journal->getId();

		import('classes.core.ServicesContainer');
		$submissionService = ServicesContainer::instance()->get('submission');

		$params = array (
			'orderBy'  => 'lastModified',
			'orderDirection' => 'ASC',
			'stageIds' => [ WORKFLOW_STAGE_ID_EDITING, WORKFLOW_STAGE_ID_PRODUCTION ],
			'count'    => -1,
			'offset'   => -1
		);
		$submissions = $submissionService->getSubmissions($contextId, $params);
		foreach ($submissions as $submission) {
			$this->addMersenneDOI($journal, $submission);
		}
	}

	/**
	 * Create and assign a new Mersenne DOI to a submission.
	 * @param $journal
	 * @param $submission
	 */
	private function addMersenneDOI($journal, $submission) {
		if ($submission->getStoredPubId('doi')) {
			return;
		}

		$doiNext = $journal->getSetting('doiCounter');
		if (is_null($doiNext)) {
			die('No DOI counter for journal #' . $journal->getId());
		}
		// bump counter for next DOI
		$journal->updateSetting('doiCounter', $doiNext + 1);

		// format Mersenne DOI
		$acro = $journal->getLocalizedAcronym();
		$doi = MERSENNE_DOI_PREFIX . "/" . strtolower($acro) . "." . (string)$doiNext;

		$submissionDao = $submission->getDAO();
		assert(!$submissionDao->pubIdExists('doi', $doi, $submission->getId(), $journal->getId()));
		printf("%s -> %s\n", $submission->getId(), $doi);
		$submissionDao->changePubId($submission->getId(), 'doi', $doi);
	}
}

$tool = new assignMersenneDOIs(isset($argv) ? $argv : array());
$tool->execute();
?>
