{**
 * templates/controllers/tab/issueEntry/form/publicationMetadataFormFields.tpl
 *
 * Copyright (c) 2016-2017 Simon Fraser University
 * Copyright (c) 2003-2017 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * This form has been stripped of fields unused by the Mersenne workflow
 * (pagination, permissions, publication scheduling).
 *}
<script type="text/javascript">
	$(function() {ldelim}
		// Attach the form handler.
		$('#publicationMetadataEntryForm').pkpHandler(
			'$.pkp.controllers.tab.issueEntry.form.IssueEntryPublicationMetadataFormHandler',
			{ldelim}
				trackFormChanges: true,
				arePermissionsAttached: {if $arePermissionsAttached}true{else}false{/if}
			{rdelim}
		);
	{rdelim});
</script>

<form class="pkp_form" id="publicationMetadataEntryForm" method="post" action="{url router=$smarty.const.ROUTE_COMPONENT op="savePublicationMetadataForm"}">
	{csrf}
	{include file="controllers/notification/inPlaceNotification.tpl" notificationId="publicationMetadataFormFieldsNotification"}

	<input type="hidden" name="submissionId" value="{$submissionId|escape}" />
	<input type="hidden" name="stageId" value="{$stageId|escape}" />
	<input type="hidden" name="waivePublicationFee" value="0" />
	<input type="hidden" name="markAsPaid" value="0" />

	{if !$publicationFeeEnabled || $publicationPayment}
		{fbvFormArea id="schedulingInformation" title="editor.article.scheduleForPublication"}
			{fbvFormSection for="schedule"}
				{if $publishedArticle}
					{assign var=issueId value=$publishedArticle->getIssueId()}
				{else}
					{assign var=issueId value=0}
				{/if}
				{fbvElement type="select" id="issueId" required=true from=$issueOptions selected=$issueId translate=false label="editor.article.scheduleForPublication.toBeAssigned"}
			{/fbvFormSection}
		{/fbvFormArea}
	{else}
		{fbvFormArea id="waivePayment" title="editor.article.payment.publicationFeeNotPaid"}
			{fbvFormSection for="waivePayment" size=$fbvStyles.size.MEDIUM}
				{fbvElement type="button" label="payment.paymentReceived" id="paymentReceivedButton" inline=true}
				{fbvElement type="button" label="payment.waive" id="waivePaymentButton" inline=true}
			{/fbvFormSection}
		{/fbvFormArea}
	{/if}

	{fbvFormButtons id="publicationMetadataFormSubmit" submitText="common.save"}
</form>
