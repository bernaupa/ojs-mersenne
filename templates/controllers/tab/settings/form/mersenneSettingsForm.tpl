{**
 * controllers/tab/settings/form/mersenneForm.tpl
 *
 * Mersenne Settings management form.
 *}
<script type="text/javascript">
	$(function() {ldelim}
		// Attach the form handler.
		$('#mersenneForm').pkpHandler('$.pkp.controllers.tab.settings.form.FileViewFormHandler',
			{ldelim}
				fetchFileUrl: {url|json_encode op='fetchFile' tab='mersenne' escape=false}
			{rdelim}
		);
	{rdelim});
</script>
<form class="pkp_form" id="mersenneForm" method="post" action="{url router=$smarty.const.ROUTE_COMPONENT op="saveFormData" tab="mersenne"}">
	{csrf}
	{include file="controllers/notification/inPlaceNotification.tpl" notificationId="mersenneNotification"}
	{include file="controllers/tab/settings/wizardMode.tpl" wizardMode=$wizardMode}

	{* In wizard mode, these fields should be hidden *}
	{if $wizardMode}
		{assign var="wizardClass" value="is_wizard_mode"}
	{else}
		{assign var="wizardClass" value=""}
	{/if}

	{assign var="articleBoilerplateFieldId" value=$uploadArticleBoilerplateLinkAction->getId()}
	{fbvFormSection label="plugins.generic.mersenne.settings.articleBoilerplate" for=$articleBoilerplateFieldId description="plugins.generic.mersenne.settings.articleBoilerplate.descripition"}
		<div id="articleBoilerplate">
			{$articleBoilerplateView}
		</div>
		<div id="{$articleBoilerplateFieldId}" class="pkp_linkActions">
			{include file="linkAction/linkAction.tpl" action=$uploadArticleBoilerplateLinkAction contextId="mersenneForm"}
		</div>
	{/fbvFormSection}

	{fbvFormSection label="plugins.generic.mersenne.settings.issueClassOptions" for="issueClassOptions" description="plugins.generic.mersenne.settings.issueClassOptions.description"}
		{fbvElement type="text" name="issueClassOptions" id="issueClassOptions" value=$issueClassOptions}
	{/fbvFormSection}
	
	{fbvFormButtons id="mersenneFormSubmit" submitText="common.save" hideCancel=true}
</form>
