{**
 * templates/controllers/grid/issues/form/issueData.tpl
 *
 * Copyright (c) 2014-2017 Simon Fraser University
 * Copyright (c) 2003-2017 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Form for creation and modification of an issue.
 *
 * This form has been modified to remove unused fields (title, description,
 * cover) and add new fields (first page) to accomodate for the Mersenne
 * workflow.
 *}

{help file="issue-management.md#edit-issue-data" class="pkp_help_tab"}
<script>
	$(function() {ldelim}
		// Attach the form handler.
		$('#issueForm').pkpHandler('$.pkp.controllers.form.AjaxFormHandler');
	{rdelim});
</script>

<form class="pkp_form" id="issueForm" method="post" action="{url op="updateIssue" issueId=$issueId}">
	{csrf}
	{include file="controllers/notification/inPlaceNotification.tpl" notificationId="issueDataNotification"}

	{if $issue && $issue->getPublished()}
		{assign var=issuePublished value=true}
	{else}
		{assign var=issuePublished value=false}
	{/if}

	{if $issuePublished}
		{fbvFormArea id="datePublishedArea" title="editor.issues.datePublished"}
			{fbvFormSection}
				{if $issuePublished}
					{fbvElement type="text" id="datePublished" value=$datePublished|date_format:$dateFormatShort size=$fbvStyles.size.SMALL class="datepicker"}
				{/if}
			{/fbvFormSection}
		{/fbvFormArea}
	{/if}


	{fbvFormArea id="identificationArea" title="editor.issues.identification"}
		{fbvFormSection}
			{fbvElement type="text" label="issue.volume" id="volume" value=$volume maxlength="40" inline=true size=$fbvStyles.size.SMALL}
			{fbvElement type="text" label="issue.number" id="number" value=$number maxlength="40" inline=true size=$fbvStyles.size.SMALL}
			{fbvElement type="text" label="issue.year" id="year" value=$year maxlength="4" inline=true size=$fbvStyles.size.SMALL}
		{/fbvFormSection}

		{fbvFormSection list=true}
			{fbvElement type="checkbox" label="issue.volume" id="showVolume" checked=$showVolume inline=true value=1}
			{fbvElement type="checkbox" label="issue.number" id="showNumber" checked=$showNumber inline=true value=1}
			{fbvElement type="checkbox" label="issue.year" id="showYear" checked=$showYear inline=true value=1}
			{fbvElement type="hidden" label="issue.title" id="showTitle" checked=$showTitle inline=true value=0}
		{/fbvFormSection}
	{/fbvFormArea}

	{fbvFormArea id="pagination" title="plugins.generic.mersenne.editor.issues.pagination"}
		{fbvElement type="text" label="plugins.generic.mersenne.issue.firstPage" id="firstPage" value=$firstPage required=true}
	{/fbvFormArea}

	{foreach from=$pubIdPlugins item=pubIdPlugin}
		{assign var=pubIdMetadataFile value=$pubIdPlugin->getPubIdMetadataFile()}
		{include file="$pubIdMetadataFile" pubObject=$issue}
	{/foreach}

	{call_hook name="Templates::Editor::Issues::IssueData::AdditionalMetadata"}

	{fbvFormButtons submitText="common.save"}
</form>
