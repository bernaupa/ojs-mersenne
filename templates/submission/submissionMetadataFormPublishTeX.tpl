{**
 * templates/submission/submissionMetadataFormPublishTeX.tpl
 *
 * Extend the submission metadata form with checkbox for the author consent to publish the LaTeX source.
 *}

{if $showPublishTeXDescription}
	{assign var=description value="plugins.generic.mersenne.publishTeX.description"}
{/if}
{fbvFormArea id="publishTeXFormArea" title="plugins.generic.mersenne.publishTeX"}
	{fbvFormSection for="publishTeX" list=true description=$description}
		{fbvElement type="checkbox" id="publishTeX" value="1" checked=$publishTeX label="plugins.generic.mersenne.publishTeX.label"}
	{/fbvFormSection}
{/fbvFormArea}
