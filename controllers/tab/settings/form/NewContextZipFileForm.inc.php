<?php

/**
 * @file controllers/tab/settings/mersenne/form/NewContextZipFileForm.inc.php
 * 
 * Copyright (c) 2014-2017 Simon Fraser University
 * Copyright (c) 2003-2017 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class NewContextZipFileForm
 * @ingroup controllers_tab_settings_mersenne_form
 *
 * @brief Form to upload a zip file.
 * 
 * @note Borrowed from pkp-lib's controllers/tab/settings/appearance/form/NewContextCssFileForm.inc.php
 */

import('lib.pkp.controllers.tab.settings.form.SettingsFileUploadForm');

class NewContextZipFileForm extends SettingsFileUploadForm {

	/**
	 * Constructor.
	 * @param $zipSettingName string
	 */
	function __construct($zipSettingName) {
		parent::__construct();
		$this->setFileSettingName($zipSettingName);
	}

	//
	// Extend methods from SettingsFileUploadForm.
	//
	/**
	 * @copydoc SettingsFileUploadForm::fetch()
	 */
	function fetch($request) {
		$params = array('fileType' => 'zip');
		return parent::fetch($request, $params);
	}


	//
	// Extend methods from Form.
	//
	/**
	 * Save the new image file.
	 * @param $request Request.
	 */
	function execute($request) {
		$temporaryFile = $this->fetchTemporaryFile($request);

		import('classes.file.PublicFileManager');
		$publicFileManager = new PublicFileManager();

		if (is_a($temporaryFile, 'TemporaryFile')) {
			$type = $temporaryFile->getFileType();
			if ($type != 'application/zip') {
				return false;
			}

			$settingName = $this->getFileSettingName();
			$uploadName = $settingName . '.zip';
			$context = $request->getContext();
			if($publicFileManager->copyContextFile($context->getAssocType(), $context->getId(), $temporaryFile->getFilePath(), $uploadName)) {
				$value = array(
					'name' => $temporaryFile->getOriginalFileName(),
					'uploadName' => $uploadName,
					'dateUploaded' => Core::getCurrentDate()
				);

				$settingsDao = $context->getSettingsDAO();
				$settingsDao->updateSetting($context->getId(), $settingName, $value, 'object');

				// Clean up the temporary file
				$this->removeTemporaryFile($request);

				return true;
			}
		}

		return false;
	}
}
?>
