<?php

/**
 * @file controllers/tab/settings/form/MersenneSettingsForm.inc.php
 *
 * @class MersenneSettingsForm
 * @ingroup plugin_generic_mersenne
 *
 * @brief Form to edit Mersenne settings.
 *
 * @see lib/pkp/controllers/tab/settings/appearance/form/PKPAppearanceForm.inc.php
 */

import('lib.pkp.classes.controllers.tab.settings.form.ContextSettingsForm');

class MersenneSettingsForm extends ContextSettingsForm {
	
	/**
	 * Constructor.
	 */
	function __construct($plugin) {
		parent::__construct(
			array(
				'issueClassOptions' => 'string',
			),
			$plugin->getTemplatePath() . 'controllers/tab/settings/form/mersenneSettingsForm.tpl',
			false
		);
	}

	//
	// Extend methods from ContextSettingsForm
	//
	/**
	 * @copydoc Form::fetch()
	 */
	function fetch($request, $params = NULL) {
		$linkAction = $this->_getFileUploadLinkAction('articleBoilerplate', 'zip', $request);

		$articleBoilerplateView = $this->renderFileView('articleBoilerplate', $request);

		$templateMgr = TemplateManager::getManager($request);
		$templateMgr->assign('uploadArticleBoilerplateLinkAction', $linkAction);

		$params = array_merge((array)$params,
			array(
				'articleBoilerplateView' => $articleBoilerplateView
			));

		return parent::fetch($request, $params);
	}

	//
	// Public methods.
	//
	/**
	 * Render a template to show details about an uploaded file in the form
	 * and a link action to delete it.
	 * @param $fileSettingName string The uploaded file setting name.
	 * @param $request Request
	 * @return string
	 */
	function renderFileView($fileSettingName, $request) {
		$file = $this->getData($fileSettingName);
		$locale = AppLocale::getLocale();

		// Check if the file is localized.
		if (!is_null($file) && key_exists($locale, $file)) {
			// We use the current localized file value.
			$file = $file[$locale];
		}

		// Only render the file view if we have a file.
		if (is_array($file)) {
			$templateMgr = TemplateManager::getManager($request);
			$deleteLinkAction = $this->_getDeleteFileLinkAction($fileSettingName, $request);

			$template = 'controllers/tab/settings/formFileView.tpl';

			$templateMgr->assign('file', $file);
			$templateMgr->assign('deleteLinkAction', $deleteLinkAction);
			$templateMgr->assign('fileSettingName', $fileSettingName);

			return $templateMgr->fetch($template);
		} else {
			return null;
		}
	}

	/**
	 * Delete an uploaded file.
	 * @param $fileSettingName string
	 * @param $request PKPRequest
	 * @return boolean
	 */
	function deleteFile($fileSettingName, $request) {
		$context = $request->getContext();
		$locale = AppLocale::getLocale();

		// Get the file.
		$file = $this->getData($fileSettingName);

		// Check if the file is localized.
		if (key_exists($locale, $file)) {
			// We use the current localized file value.
			$file = $file[$locale];
		} else {
			$locale = null;
		}

		// Deletes the file and its settings.
		import('classes.file.PublicFileManager');
		$publicFileManager = new PublicFileManager();
		if ($publicFileManager->removeContextFile($context->getAssocType(), $context->getId(), $file['uploadName'])) {
			$settingsDao = $context->getSettingsDao();
			$settingsDao->deleteSetting($context->getId(), $fileSettingName, $locale);
			return true;
		} else {
			return false;
		}
	}

	//
	// Private helper methods
	//
	/**
	 * Get a link action for file upload.
	 * @param $settingName string
	 * @param $fileType string The uploaded file type.
	 * @param $request Request
	 * @return LinkAction
	 */
	function _getFileUploadLinkAction($settingName, $fileType, $request) {
		$router = $request->getRouter();
		import('lib.pkp.classes.linkAction.request.AjaxModal');
		import('lib.pkp.classes.linkAction.LinkAction');
		return new LinkAction(
			'uploadFile-' . $settingName,
			new AjaxModal(
				$router->url(
					$request, null, null, 'showFileUploadForm', null, array(
						'fileSettingName' => $settingName,
						'fileType' => $fileType
					)
				),
				__('common.upload'),
				'modal_add_file'
			),
			__('common.upload'),
			'add'
		);
	}

	/**
	 * Get the delete file link action.
	 * @param $settingName string File setting name.
	 * @param $request Request
	 * @return LinkAction
	 */
	function _getDeleteFileLinkAction($settingName, $request) {
		$router = $request->getRouter();
		import('lib.pkp.classes.linkAction.request.RemoteActionConfirmationModal');

		return new LinkAction(
			'deleteFile-' . $settingName,
			new RemoteActionConfirmationModal(
				$request->getSession(),
				__('common.confirmDelete'), null,
				$router->url(
					$request, null, null, 'deleteFile', null, array(
						'fileSettingName' => $settingName,
						'tab' => 'mersenne'
					)
				),
				'modal_delete'
			),
			__('common.delete'),
			null
		);
	}
}
?>
