<?php

/**
 * @file controllers/tab/settings/MersenneSettingsHandler.inc.php
 *
 * @class MersenneSettingsHandler
 * @ingroup plugin_generic_mersenne
 *
 * @brief Handle AJAX operations for the Mersenne tab.
 * 
 * @see lib/pkp/classes/controllers/tab/settings/SettingsTabHandler.inc.php
 * @see controllers/tab/settings/PublicationSettingsTabHandler.inc.php
 */

import('classes.handler.Handler');

class MersenneSettingsHandler extends Handler {
	/** @var TranslatorPlugin The translator plugin */
	static $plugin;

	/**
	 * Set the translator plugin.
	 * @param $plugin StaticPagesPlugin
	 */
	static function setPlugin($plugin) {
		self::$plugin = $plugin;
	}

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
	}

	//
	// Overridden template methods
	//
	/**
	 * @copydoc Handler::initialize()
	 */
	function initialize($request, $args = null) {
		parent::initialize($request, $args);
		$this->addRoleAssignment(ROLE_ID_MANAGER,
			array(
				'fetch',
				'saveFormData',
				// file view operations
				'showFileUploadForm',
				'uploadFile',
				'saveFile',
				'deleteFile',
				'fetchFile'
			)
		);
	}

	//
	// Public handler methods
	//
	/**
	 * Display the Mersenne settings.
	 * @param $args array
	 * @param $request Request
	 * @return JSONMessage JSON object
	 */
	function fetch($args, $request) {
		$mersenneForm = $this->getForm();
		$mersenneForm->initData($request);
		return new JSONMessage(true, $mersenneForm->fetch($request));
	}
	
	/**
	 * Save the Mersenne settings form.
	 * @param $args array
	 * @param $request Request
	 * @return \JSONMessage JSON object
	 */
	function saveFormData($args, $request) {
		$mersenneForm = $this->getForm();
		// try to save the form data
		$mersenneForm->readInputData($request);
		if ($mersenneForm->validate()) {
			$result = $mersenneForm->execute($request);
			if ($result !== false) {
				$notificationManager = new NotificationManager();
				$user = $request->getUser();
				$notificationManager->createTrivialNotification($user->getId());
			}
			return new JSONMessage();
		} else {
			return new JSONMessage(FALSE);
		}
	}

		/**
	 * Show the upload image form.
	 * @param $request Request
	 * @param $args array
	 * @return \JSONMessage JSON object
	 */
	function showFileUploadForm($args, $request) {
		$fileUploadForm = $this->getFileUploadForm($request);
		$fileUploadForm->initData($request);

		return new JSONMessage(true, $fileUploadForm->fetch($request));
	}

	/**
	 * Upload a new file.
	 * @param $args array
	 * @param $request PKPRequest
	 * @return \JSONMessage JSON object
	 */
	function uploadFile($args, $request) {
		$fileUploadForm = $this->getFileUploadForm($request);

		$temporaryFileId = $fileUploadForm->uploadFile($request);

		if ($temporaryFileId !== false) {
			$json = new JSONMessage();
			$json->setAdditionalAttributes(array(
				'temporaryFileId' => $temporaryFileId
			));
			return $json;
		} else {
			return new JSONMessage(false, __('common.uploadFailed'));
		}
	}

	/**
	 * Save an uploaded file.
	 * @param $args array
	 * @param $request PKPRequest
	 * @return \JSONMessage JSON object
	 */
	function saveFile($args, $request) {
		$fileUploadForm = $this->getFileUploadForm($request);
		$fileUploadForm->readInputData($request);

		if ($fileUploadForm->validate()) {
			if ($fileUploadForm->execute($request)) {
				// Generate a JSON message with an event
				$settingName = $request->getUserVar('fileSettingName');
				return DAO::getDataChangedEvent($settingName);
			}
		}
		return new JSONMessage(false, __('common.invalidFileType'));
	}
	
	/**
	 * Deletes a journal image.
	 * @param $args array
	 * @param $request PKPRequest
	 * @return string
	 */
	function deleteFile($args, $request) {
		$settingName = $request->getUserVar('fileSettingName');

		$tabForm = $this->getForm();
		$tabForm->initData($request);

		if ($request->checkCSRF() && $tabForm->deleteFile($settingName, $request)) {
			return DAO::getDataChangedEvent($settingName);
		} else {
			return new JSONMessage(false);
		}
	}

	/**
	 * Fetch a file that has been uploaded.
	 *
	 * @param $args array
	 * @param $request Request
	 * @return \JSONMessage JSON object
	 */
	function fetchFile($args, $request) {
		// Get the setting name.
		$settingName = $args['settingName'];

		// Try to fetch the file.
		$tabForm = $this->getForm();
		$tabForm->initData($request);

		$renderedElement = $tabForm->renderFileView($settingName, $request);

		$json = new JSONMessage();
		if ($renderedElement == false) {
			$json->setAdditionalAttributes(array('noData' => $settingName));
		} else {
			$json->setElementId($settingName);
			$json->setContent($renderedElement);
		}
		return $json;
	}

	//
	// Private helper methods.
	//
	/**
	 * Returns a file upload form.
	 * @param $request Request
	 * @return Form
	 */
	private function getFileUploadForm($request) {
		$settingName = $request->getUserVar('fileSettingName');
		$fileType = $request->getUserVar('fileType');

		import('plugins.generic.mersenne.controllers.tab.settings.form.NewContextZipFileForm');
		$fileUploadForm = new NewContextZipFileForm($settingName);

		return $fileUploadForm;
	}

	/**
	 * Get an instance of the Mersenne form.
	 * @return \MersenneSettingsForm
	 */
	private function getForm() {
		import('plugins.generic.mersenne.controllers.tab.settings.form.MersenneSettingsForm');
		return new MersenneSettingsForm(self::$plugin);
	}

}

?>
