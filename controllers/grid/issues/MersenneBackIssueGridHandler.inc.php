<?php

/**
 * @file controllers/grid/issues/MersenneBackIssueGridHandler.inc.php
 *
 * @class MersenneBackIssueGridHandler
 * @ingroup plugins_generic_mersenne
 *
 * @brief Handle back issues grid requests.
 */

import('controllers.grid.issues.BackIssueGridHandler');

class MersenneBackIssueGridHandler extends BackIssueGridHandler {
	//
	// Overridden methods from GridHandler
	//
	/**
	 * @copydoc GridHandler::getRowInstance()
	 * @return MersenneIssueGridRow
	 */
	protected function getRowInstance() {
		// replace the grid row to add action
		import('plugins.generic.mersenne.controllers.grid.issues.MersenneIssueGridRow');
		return new MersenneIssueGridRow();
	}

}

?>
