<?php

/**
 * @file controllers/grid/issues/MersenneFutureIssueGridHandler.inc.php
 *
 * @class MersenneFutureIssueGridHandler
 * @ingroup plugins_generic_mersenne
 *
 * @brief Handle future issues grid requests.
 */

import('controllers.grid.issues.FutureIssueGridHandler');

class MersenneFutureIssueGridHandler extends FutureIssueGridHandler {
	//
	// Overridden methods from GridHandler
	//
	/**
	 * @copydoc GridHandler::getRowInstance()
	 * @return MersenneIssueGridRow
	 */
	protected function getRowInstance() {
		// replace the grid row to add action
		import('plugins.generic.mersenne.controllers.grid.issues.MersenneIssueGridRow');
		return new MersenneIssueGridRow();
	}

}

?>
