<?php

/**
 * @file controllers/grid/issues/MersenneIssueGridRow.inc.php
 *
 * @class MersenneIssueGridRow
 * @ingroup plugins_generic_mersenne
 *
 * @brief Redefinition of the grid issue row to add action.
 */

import('controllers.grid.issues.IssueGridRow');

class MersenneIssueGridRow extends IssueGridRow {

	//
	// Overridden template methods
	//
	/**
	 * Configure the grid row.
	 * @param $request PKPRequest
	 */
	function initialize($request) {
		parent::initialize($request);
		$router = $request->getRouter();
		$issueId = $this->getId();
		import('lib.pkp.classes.linkAction.request.RedirectAction');
		$this->addAction(
			new LinkAction(
				'downloadIssue',
				new RedirectAction($router->url($request, null, 'grid.issues.MersenneIssueGridHandler', 'downloadIssue', null, array('issueId' => $issueId))),
				__('plugins.generic.mersenne.grid.action.downloadIssue'),
				'information')
		);
	}

}

?>
