<?php

/**
 * @file controllers/grid/issues/MersenneIssueGridHandler.inc.php
 *
 * @class MersenneIssueGridHandler
 * @ingroup plugins_generic_mersenne
 *
 * @brief Handle future and back issues grid requests.
 */

import('classes.controllers.grid.issues.IssueGridHandler');

class MersenneIssueGridHandler extends IssueGridHandler {
	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		$this->addRoleAssignment(
			array(ROLE_ID_MANAGER),
			array(
				'downloadIssue'
			)
		);
	}

	//
	// Public operations
	//
	/**
	 * Build and return an archive for an issue from its definition.
	 * @param array $args the arguments
	 * @param PKPRequest $request the request
	 */
	function downloadIssue($args, $request) {
		$issue = $this->getAuthorizedContextObject(ASSOC_TYPE_ISSUE);
		$journal = $request->getJournal();

		$z = new ZipArchive();
		$filename = tempnam('/tmp', 'mersenne-');
		$res = $z->open($filename, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		$acro = $journal->getLocalizedAcronym();
		$year = $issue->getYear();
		$volume = $issue->getShowVolume() ? $issue->getVolume() : '';
		$number = $issue->getShowNumber() ? $issue->getNumber() : '';
		$issueIdentifier = implode('_', [ $acro, $year, '', $volume, $number ]);
		
		$firstPage = $issue->getData('firstPage');
		
		$prefix = $issueIdentifier;
		$z->addEmptyDir($prefix);
		
		$publishedArticleDao = DAORegistry::getDAO('PublishedArticleDAO');
		$publishedArticles = $publishedArticleDao->getPublishedArticles($issue->getId());
		if (isset($publishedArticles) && !empty($publishedArticles)) {
			foreach ($publishedArticles as $publishedArticle) {
				$articles[] = $this->_addArticle($publishedArticle, $z, $prefix);
			}
		}

		$classOptions = $journal->getSetting('issueClassOptions');

		// helper function for including articles
		$includearticles = function ($articles) {
			return implode(PHP_EOL, array_map(function ($article) { return "\\includearticle{{$article}}"; }, $articles));
		};
		$issueTeX = <<<EOT
\\documentclass[{$classOptions}]{cedram}
\\IssueInfo{{$volume}}{{$number}}{}{{$year}}
\\SetFirstPage{{$firstPage}}

\\begin{document}
\\makefront

{$includearticles($articles)}

\\makeback
\\end{document}
EOT;
		$res = $z->addFromString(implode('/', [ $prefix, $prefix.'.tex' ]), $issueTeX);

		$z->close();
		
		import('lib.pkp.classes.file.FileArchive');
		$fileManager = new FileManager();
		$fileManager->downloadFile($filename, 'application/x-zip', false, $issueIdentifier.'.zip');
		$fileManager->deleteFile($filename);
	}

	/**
	 * Include an article in a issue archive.
	 * @param PublishedArticle $article the article to include
	 * @param ZipArchive $zip the receiving issue archive
	 * @param string $prefix the base path in the archive for article files
	 * @return string the id of the article added to the archive
	 */
	private function _addArticle($article, $zip, $prefix) {
		import('plugins.generic.mersenne.MersennePlugin'); // import constants
		
		$articleGalleyDao = DAORegistry::getDAO('ArticleGalleyDAO');
		$sourceGalley = array_shift(
			array_filter(
				$articleGalleyDao->getBySubmissionId($article->getId())->toArray(),
				function ($aG) { return $aG->getLabel() === MERSENNE_ARTICLE_SOURCE_GALLEY; }
			));
		if (empty($sourceGalley)) {
			fatalError("No source galley found for submission #" . $article->getId());
		}

		// select the source archive for article
		$sourceFile = $sourceGalley->getFile();
		if (!isset($sourceFile) || $sourceFile->getDocumentType() != DOCUMENT_TYPE_ZIP) {
			fatalError("No article archive found for submission #" . $article->getId());
		}
		
		$id = null;
		// copy from source zip
		$z = new ZipArchive();
		$res = $z->open($sourceFile->getFilePath(), ZipArchive::CHECKCONS);
		for ($i = 0; $i < $z->numFiles; $i++) {
			$name = $z->getNameIndex($i);
			if (substr($name, -1) == '/') {
					// discover the identifier from directory name
					if (empty($id)) $id = explode('/', $name)[0];

					// skip the creation of directories
					continue;
			}
			$res = $zip->addFromString(implode('/', [ $prefix, $name ]), $z->getFromIndex($i));
		}
		$z->close();

		// failover for identifier
		if (empty($id)) $id = pathinfo($sourceFile->getOriginalFilename(), PATHINFO_FILENAME);

		return $id;
	}
	
}

?>
