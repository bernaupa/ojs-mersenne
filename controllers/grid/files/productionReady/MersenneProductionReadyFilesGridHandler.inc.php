<?php

/**
 * @file controllers/grid/files/productionReady/MersenneProductionReadyFilesGridHandler.inc.php
 *
 * @class MersenneProductionReadyFilesGridHandler
 * @ingroup plugins_generic_mersenne
 *
 * @brief Handler adding a button for preparing all files as an initial article archive.
 */

import('lib.pkp.controllers.grid.files.productionReady.ProductionReadyFilesGridHandler');

class MersenneProductionReadyFilesGridHandler extends ProductionReadyFilesGridHandler {
	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		$this->addRoleAssignment(
			array(ROLE_ID_MANAGER, ROLE_ID_SUB_EDITOR, ROLE_ID_ASSISTANT),
			array('prepareArticle')
		);
	}

	/**
	 * @copydoc PKPHandler::initialize()
	 */
	function initialize($request) {
		parent::initialize($request);
		
		if ($this->hasGridDataElements($request)) {
			// TODO test if there is already a prepared article
			$files = $this->getGridDataElements($request);
			$submission = $this->getSubmission();
			$stageId = $this->getStageId();
			$linkParams = array('submissionId' => $submission->getId(), 'stageId' => $stageId);
			$filesIdsAndRevisions = $this->_getFilesIdsAndRevisions($files);
			$linkParams['filesIdsAndRevisions'] = $filesIdsAndRevisions;
			
			import('lib.pkp.classes.linkAction.request.PostAndRedirectAction');
			$prepareArticleAction = new LinkAction(
					'prepareArticle',
					new PostAndRedirectAction(
							$request->url(null, 'api.file.FileApiHandler', 'enableLinkAction', null, $linkParams),
							$request->url(null, null, 'prepareArticle', null, $linkParams)
					),
					__('plugins.generic.mersenne.action.prepareArticle'),
					'getPackage'
			);
			$this->addAction($prepareArticleAction, GRID_ACTION_POSITION_BELOW);			
		}
	}
	
	// Public Grid Actions
	/**
	 * Return an article archive from production ready files.
	 * @param $args array
	 * @param $request Request
	 * @return JSONMessage JSON object
	 */
	function prepareArticle($args, $request) {
		$submission = $this->getSubmission();
		$submissionFiles = array_map(function($fileData) { return $fileData['submissionFile']; }, $this->getFilesToDownload($request));
		$context = $request->getContext();

		$fileManager = new PublicFileManager();
		$publicFilesDir = $fileManager->getJournalFilesPath($context->getId());
		$boilerplatePath = implode('/', [ $publicFilesDir, 'articleBoilerplate.zip' ] );
		if (!$fileManager->fileExists($boilerplatePath)) {
			fatalError('Article boilerplate not found!');
		}

		// build a name for article from journal acro, author name and submission id
		$id = self::_articleId($submission, $context);

		$zip = new ZipArchive();
		$filename = tempnam('/tmp', 'mersenne-');
		$zip->open($filename, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		// copy the contents of the boilerplate
		$z = new ZipArchive();
		$z->open($boilerplatePath, ZipArchive::CHECKCONS);
		$base = null;
		for ($i = 0; $i < $z->numFiles; $i++) {
			$name = $z->getNameIndex($i);
			$explodedName = explode("/", $name);

			// find out the base directory of the boilerplate and strip
			if (!$base) {
				$base = $explodedName[0];
			}
			array_shift($explodedName);
			// skip creation of directories
			if (substr($name, -1) == '/') {
				continue;
			}

			// rename the LaTeX main file of the boilerplate
			if ($name == $base . '/' . $base . '.tex') {
				$localname = implode('/', [ $id, $id . '.tex' ]);
			} else {
				$localname = implode('/', array_merge([ $id ], $explodedName));
			}
			$zip->addFromString($localname, $z->getFromIndex($i));
		}
		$z->close();

		// embed the production ready files
		$zip->addEmptyDir(implode('/', [ $id, 'originaux' ]));
		foreach ($submissionFiles as $submissionFile) {
			$zip->addFile(
					$submissionFile->getFilePath(),
					implode('/', [ $id, 'originaux', $submissionFile->getOriginalFileName() ])
			);
		}

		// generate the submission info file
		$zip->addFromString(
				implode('/', [ $id, $id.'.cdrdoidates' ]),
				self::_cdrdoidates($submission)
		);

		$zip->close();

		$fileManager->downloadFile($filename, 'application/x-zip', false, $id . '.zip');
		$fileManager->deleteFile($filename);
	}

	//
	// Private helper methods.
	//
	/**
	 * Return a string with all files ids and revisions.
	 * @param $files array The files that will be downloaded.
	 * @return string
	 */
	private function _getFilesIdsAndRevisions($files) {
		$filesIdsAndRevisions = null;
		foreach ($files as $fileData) {
			$file =& $fileData['submissionFile'];
			$fileId = $file->getFileId();
			$revision = $file->getRevision();
			$filesIdsAndRevisions .= $fileId . '-' . $revision . ';';
			unset($file);
		}

		return $filesIdsAndRevisions;
	}

	/**
	 * Helper for building an article identifier.
	 * @param $submission Submission the submission
	 * @param $context Context the parent journal
	 * @return string an identifier for the article (JOURNAL_authorname_SubmissionId)
	 */
	static private function _articleId($submission, $context) {
		// build a name for article from journal acro, author name and submission id
		$author = array_shift($submission->getAuthors());
		$authorName = $author->getLastName();
		// simplify author name for path in archive
		$authorName = preg_replace('~[^\pL\d]+~u', '-', $authorName);
		$authorName = iconv('utf-8', 'us-ascii//TRANSLIT', $authorName);
		$authorName = preg_replace('~[^-\w]+~', '', $authorName);
		$authorName = trim($authorName, '-');
		return implode('_',[ $context->getLocalizedAcronym(), $authorName, $submission->getId() ]);
	}

	/**
	 * Get submission date.
	 * @param $submission Submission the submission
	 * @return \DateTime
	 */
	static private function _getSubmissionDateSubmitted($submission) {
		return new DateTime($submission->getDateSubmitted());
	}

	/**
	 * Get date the submission has been accepted.
	 * @param Submission $submission the submission
	 * @return \DateTime or null if not accepted
	 */
	static private function _getSubmissionDateAccepted($submission) {
		$editDecisionDao = DAORegistry::getDAO('EditDecisionDAO');
		$editDecisions = $editDecisionDao->getEditorDecisions($submission->getId());
		// look for an accept decision
		do {
			$editorDecision = array_pop($editDecisions);
		} while ($editorDecision && $editorDecision['decision'] != SUBMISSION_EDITOR_DECISION_ACCEPT);

		if (!$editorDecision) {
			return null;
		}

		return new DateTime($editorDecision['dateDecided']);
	}

	/**
	 * Get date of last revision on round from submission.
	 * @param Submission $submission the submission
	 * @param int $round the round number
	 * @return \DateTime or null if no revision
	 */
	static private function _getSubmissionDateRevised($submission, $round = 1) {
		$reviewRoundDao = DAORegistry::getDAO('ReviewRoundDAO');
		$reviewRound = $reviewRoundDao->getBySubmissionId($submission->getId(), null, $round)->next();
		if (!$reviewRound) {
			return null;
		}

		$submissionFileDao = DAORegistry::getDAO('SubmissionFileDAO');
		$submissionFiles = $submissionFileDao->getLatestRevisionsByReviewRound($reviewRound, SUBMISSION_FILE_REVIEW_REVISION);
		if (!$submissionFiles) {
			return null;
		}

		return array_reduce($submissionFiles, function ($carry, $item) { return max($carry, new DateTime($item->getDateUploaded())); });
	}
	
	/**
	 * Format submission dates and DOI in LaTeX.
	 * @param Submission $submission the submission to dump data from
	 * @return string submission metadata as .cdrdoidates file contents
	 */
	static private function _cdrdoidates($submission) {
		$doi = $submission->getStoredPubId('doi');
		$cdrdoidates = "\DOI{{$doi}}\n";

		// TODO dateposted, datererevised, daterevisedaccepted
		$dates = array(
			'received' => self::_getSubmissionDateSubmitted($submission),
			'revised'  => self::_getSubmissionDateRevised($submission),
			'rerevised'=> self::_getSubmissionDateRevised($submission, 2),
			'accepted' => self::_getSubmissionDateAccepted($submission)
		);
		foreach($dates as $type=>$date) {
			if ($date) {
				$cdrdoidates .= "\date{$type}{{$date->format('Y-m-d')}}\n";
			}
		}

		HookRegistry::call('MersennePlugin::exportSubmissionMetadata', array($submission, &$cdrdoidates));

		return $cdrdoidates;
	}
	
}

?>
