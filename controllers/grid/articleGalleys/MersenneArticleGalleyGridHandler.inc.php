<?php

/**
 * @file controllers/grid/articleGalley/MersenneArticleGalleyGridHandler.inc.php
 *
 * @class MersenneArticleGalleyGridHandler
 * @ingroup plugins_generic_mersenne
 *
 * @brief Handler adding a button for prepared article as article galleys.
 */

import('controllers.grid.articleGalleys.ArticleGalleyGridHandler');

class MersenneArticleGalleyGridHandler extends ArticleGalleyGridHandler {
	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		$this->addRoleAssignment(
			array(ROLE_ID_MANAGER, ROLE_ID_SUB_EDITOR, ROLE_ID_ASSISTANT),
			array('addSourceGalley'));
	}

	/**
	 * @copydoc PKPHandler::initialize()
	 */
	function initialize($request) {
		parent::initialize($request);

		$router = $request->getRouter();
		$userRoles = $this->getAuthorizedContextObject(ASSOC_TYPE_USER_ROLES);
		if (0 != count(array_intersect($userRoles, array(ROLE_ID_MANAGER, ROLE_ID_SUB_EDITOR, ROLE_ID_ASSISTANT)))) {
			import('lib.pkp.classes.linkAction.request.AjaxAction');
			$actionArgs = $this->getRequestArgs();
			$actionArgs['label'] = MERSENNE_ARTICLE_SOURCE_GALLEY;
			$this->addAction(new LinkAction(
				'addSourceGalley',
				new AjaxAction($router->url($request, null, null, 'addGalley', null, $actionArgs)),
				__('plugins.generic.mersenne.grid.action.addGalley', array('label' => MERSENNE_ARTICLE_SOURCE_GALLEY)),
				'add_item'
			));
		}
	}

	/**
	 * Shortcut for adding a galley with a predefined label.
	 * @param $args array
	 * @param $request PKPRequest
	 * @return JSONMessage JSON object
	 */
	function addGalley($args, $request) {
		$submissionId = $this->getSubmission()->getId();
		$label = $request->getUserVar('label');
		import('plugins.generic.mersenne.MersennePlugin'); // import constants
		if (!in_array($label, array(MERSENNE_ARTICLE_SOURCE_GALLEY))) {
			return parent::addGalley($args, $request);
		}

		$articleGalleyDao = DAORegistry::getDAO('ArticleGalleyDAO');

		// search for an existing galley
		$articleGalley = array_shift(
			array_filter(
				$articleGalleyDao->getBySubmissionId($submissionId)->toArray(),
				function ($aG) use ($label) { return $aG->getLabel() === $label; }
			));

		if (!$articleGalley) {
			// no source galley, create new one
			$articleGalley = $articleGalleyDao->newDataObject();
			$articleGalley->setSubmissionId($this->getSubmission()->getId());
			$articleGalley->setLabel($label);
			$articleGalleyDao->insertObject($articleGalley);

			// continue as in ArticleGalleyGridHandler::updateGalley()
			$notificationMgr = new NotificationManager();
			$notificationMgr->updateNotification(
				$request,
				array(NOTIFICATION_TYPE_ASSIGN_PRODUCTIONUSER, NOTIFICATION_TYPE_AWAITING_REPRESENTATIONS),
				null,
				ASSOC_TYPE_SUBMISSION,
				$submissionId
			);
		}

		return DAO::getDataChangedEvent($articleGalley->getId());
	}

	function getActions($position = GRID_ACTION_POSITION_ABOVE) {
		$actions = parent::getActions($position);
		if ($position == GRID_ACTION_POSITION_ABOVE) {
			unset($actions['addGalley']);
		}
		return $actions;
	}

}