# Mersenne plugin for OJS


## About

This plugin customizes [OJS](https://pkp.sfu.ca/ojs/) to the [Mersenne](https://www.centre-mersenne.org) workflow.

More specifically this plugin:
 - adapts the scheduling for publication form and the issue creation;
 - prepares each article to be processed by layout editors from the production-ready files;
 - assembles the articles of an issue according to the table of contents and supplementary issue metadata;
 - creates [DOI](https://en.wikipedia.org/wiki/Digital_object_identifier) on acceptation of the submission.


## System requirements

This plugin has been tested on OJV version 3.1.0.0.

It depends on the [PHP zip extension](http://php.net/manual/en/book.zip.php).


## Installation

To install:
 - unpack the plugin archive to OJS's plugins/generic directory as a new 'mersenne' directory;
 - enable the plugin by ticking the checkbox for "Mersenne" in the set of generic plugins available ('Management' > 'Website Settings' > 'Plugins' > 'Generic Plugins').

**Note**:
Make sure the name of the directory for the plugin is 'mersenne', not 'ojs-mersenne'.

## Features

### Production helper

The plugin provides a way for the Layout Editor in charge of a submission to download all production-ready files in a prepared archive.

This archive is built from a boilerplate that is journal-specific and declared on the plugin settings page ('Settings' > 'Workflow' > 'Mersenne' > 'Article Boilerplate').

The boilerplate is expected to be a ZIP file containing a single directory with at least a '.tex' file inside. The basename of that file should be the same as the name of the directory.

For example the contents of the archive could be:
```
journal/
journal/journal.tex
```

The plugin automatically:
 - proposes an identifier for the article;
 - includes additional metadata (submission dates, DOI) in a '.cdrdoidates' file that should not be modified.

**Note**:
A hook ('MersennePlugin::exportSubmissionMetadata') is provided to interfere with the contents of the '.cdrdoidates'.
In particular a plugin can register to the hook and use the generated file to forward extra data from the submission in OJS into the article archive.


### LaTeX article galley

The plugins provides a short cut for adding a LaTeX source archive as an article galley when the layout and proofreading has been done.

The structure of the archive is validated against the requirements of an article archive for the Mersenne workflow.


### Publication helper

The plugin assembles as a Mersenne volume an issue from its table of contents and its declaration (volume identification, pagination clues...).

The 'Download Issue' action ('Issues' > 'Future Issues' or 'Back Issues') creates a ZIP archive from all the articles of the issue and a [LaTeX](https://en.wikipedia.org/wiki/LaTeX) volume file compliant with the Mersenne publication workflow.

The document class options of the LaTeX volume file created can be specified from the plugin settings page ('Settings' > 'Workflow' > 'Mersenne' > 'Issue Document Class Options').


### DOI creation

The plugin automatically creates a DOI for submission on acceptation as a public identifier for submission (`pub-id::doi`).

The DOIs will follow the Mersenne DOI scheme with:
 - the Mersenne prefix (10.5802);
 - the Mersenne suffix scheme using the journal initials and an incremental counter.

That counter has to be specifically defined for each journal and carefully initialized to avoid conflict with potentially existing DOIs.

For example, the following SQL statement set the counter for journal JOURNAL to 1 (MySQL database):
```sql
INSERT INTO journal_settings VALUES 
    (
		( SELECT journal_id FROM ( SELECT * FROM journal_settings WHERE setting_name='acronym' AND setting_value='JOURNAL' ) AS x ),
		'',
		'doiCounter',
		'1',
		'int'
	);
```

No DOI is created if the counter is not defined.

**Note**:
The command line tool `assignMersenneDOIs.php` retroactively creates DOI for submissions that have already been accepted and that _have no DOI assigned yet_.


### Permission to publish LaTeX source

For accessibility reason, the journal encourages authors to allow the download of the copyedited LaTeX source files for the published articles along the regular PDF files.

A checkbox is proposed during the submission process to obtain the consent of the author to the publication.

The choice is echoed in the article archive from the [production helper](#production-helper) and handled by the Mersenne publication workflow.
