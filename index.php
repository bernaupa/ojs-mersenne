<?php

/**
 * @defgroup plugins_generic_mersenne Mersenne plugin
 */
 
/**
 * @file index.php
 *
 * @ingroup plugins_generic_mersenne
 * @brief Wrapper for Mersenne plugin.
 *
 */
require_once('MersennePlugin.inc.php');

return new MersennePlugin();

?>
