<?php

/**
 * @file tests/functional/MersenneFunctionalTest.php
 *
 *
 * @class MersenneFunctionalTest
 * @ingroup plugins_generic_mersenne
 *
 * @brief Functional tests for the Mersenne plugin.
 */

import('tests.ContentBaseTestCase');

class MersenneFunctionalTest extends ContentBaseTestCase {
	/** @var int Initial value of DOI counter for DOI creation */
	protected $doiCounter = 1;

	/** @var string Title of a hand-picked submission in review stage from test database */
	protected $title = 'The influence of lactation on the quantity and quality of cashmere production';
	
	/** @var string The author username for submission */
	protected $author = 'ccorino';

	/**
	 * @copydoc WebTestCase::getAffectedTables
	 */
	protected function getAffectedTables() {
		return PKP_TEST_ENTIRE_DB;
	}

	/**
	 * @see WebTestCase::setUp()
	 */
	protected function setUp() {
		parent::setUp();

		$this->start();

		$this->enablePlugin();

		// enable DOI creation by setting DOI counter
		$journalDao = DAORegistry::getDAO('JournalDAO'); /* @var journalDao JournalDAO */
		$journal = $journalDao->getById(1);
		$journal->updateSetting('doiCounter', $this->doiCounter, 'int');
	}

	/**
	 * Enable the plugin
	 */
	private function enablePlugin() {
		$this->logIn('admin', 'admin');

		$this->waitForElementPresent($selector = 'link=Website');
		$this->clickAndWait($selector);
		$this->click('link=Plugins');

		// find the plugin
		$this->waitForElementPresent($selector = '//input[starts-with(@id, \'select-cell-mersenneplugin-enabled\')]');
		// enable if necessary
		if (!$this->isChecked($selector)) {
			// do enable the plugin
			$this->click($selector);
			$this->waitForElementPresent('//div[contains(.,\'The plugin "Mersenne" has been enabled.\')]');
		}

		$this->logOut();
	}

	/**
	 * Upload a file as article boilerplate.
	 * @param $file string the path to the file to upload
	 */
	private function uploadArticleBoilerplate($file) {
		$this->click('css=[id^=mersenneForm-uploadFile-articleBoilerplate-button-]');

		$this->waitForElementPresent('css=div.pkp_modal');
		$this->uploadFile($file);
		$this->waitForNotAttribute('css=[id=temporaryFileId]@value', '');
		$this->click('css=button:contains(\'OK\')');
		$this->waitForElementNotPresent('css=div.pkp_modal');
	}

	/**
	 * Retrieve an issue file.
	 *
	 * @param string $url the download URL
	 * @param string $user
	 * @param string $output the name of the output file
	 */
	private function download($url, $user, $output) {
		// use curl to actually get the file contents
		$curlCh = curl_init ();
		curl_setopt($curlCh, CURLOPT_POST, true);
		$cookies = tempnam (sys_get_temp_dir(), 'curlcookies');
		// log in
		$loginUrl = self::$baseUrl . '/index.php/publicknowledge/login/signIn';
		$loginParams = array(
			'username' => $user,
			'password' => $user . $user
		);
		curl_setopt($curlCh, CURLOPT_URL, $loginUrl);
		curl_setopt($curlCh, CURLOPT_POSTFIELDS, $loginParams);
		curl_setopt($curlCh, CURLOPT_COOKIEJAR, $cookies);
		$this->assertTrue(curl_exec($curlCh));

		$file = fopen($output, 'w');
		// download to local temporary file
		curl_setopt($curlCh, CURLOPT_POST, false);
		curl_setopt($curlCh, CURLOPT_URL, $url);
		curl_setopt($curlCh, CURLOPT_HEADER, false);
		curl_setopt($curlCh, CURLOPT_FILE, $file);
		$this->assertTrue(curl_exec($curlCh));
		fclose($file);
		// clean up
		curl_close($curlCh);
		unlink($cookies);
	}

	/**
	 * Validate a Mersenne article archive.
	 *
	 * @param string $articleFile the name of the article archive
	 * @param string $productionReadyFiles the names of the article source files
	 * @param boolean $noPublishTex whether to expect a NoPublishTeX option
	 */
	private function validateArticleArchive($articleFile, $productionReadyFiles = array(), $noPublishTeX = false) {
		$z = new \ZipArchive();
		$this->assertNotFalse($z->open($articleFile, ZipArchive::CHECKCONS));

		// extract article identifier from directory name
		$name = $z->getNameIndex(1, ZipArchive::FL_UNCHANGED);
		$this->assertNotFalse($name);
		$dirname = explode('/', $name)[0];

		// check article LaTeX file
		$filename = $dirname . '/' . $dirname . '.tex';
		$articleTex = $z->getFromName($filename);
		$this->assertNotFalse($articleTex);

		// check article cdrdoidates file
		$filename = $dirname . '/' . $dirname . '.cdrdoidates';
		$articleCdrdoidates = $z->getFromName($filename);
		$this->assertNotFalse($articleCdrdoidates);
		$this->assertRegExp('/\\\\datereceived\\{\\d{4}-\\d{2}-\\d{2}.*\\}/', $articleCdrdoidates);
		$this->assertRegExp('/\\\\dateaccepted\\{\\d{4}-\\d{2}-\\d{2}.*\\}/', $articleCdrdoidates);
		$noPublishTeXNeedle = "\ItHasNoTeXPublished";
		if ($noPublishTeX) {
			$this->assertContains($noPublishTeXNeedle, $articleCdrdoidates);
		} else {
			$this->assertNotContains($noPublishTeXNeedle, $articleCdrdoidates);
		}

		// check original source included
		foreach ($productionReadyFiles as $file) {
			$filename = $dirname . '/originaux/' . $file;
			$articleOriginaux = $z->getFromName($filename);
			$this->assertNotFalse($articleOriginaux);
		}

		$this->assertTrue($z->close());
	}

	/**
	 * Validate a Mersenne issue archive.
	 *
	 * @param string $issueFile the name of the issue archive
	 * @param string $firstPage the first page of the issue
	 * @param string $volume the volume number
	 * @param string $issue the issue number
	 * @param string $year the publication year
	 * @param string $documentClassOptions the issue document class options
	 */
	private function validateIssueArchive($issueFile, $firstPage, $volume = '', $number = '', $year = '', $documentClassOptions = '') {
		$z = new \ZipArchive();
		$this->assertNotFalse($z->open($issueFile, ZipArchive::CHECKCONS));

		// extract issue identifier from directory name
		$name = $z->getNameIndex(1, ZipArchive::FL_UNCHANGED);
		$this->assertNotFalse($name);
		$dirname = explode('/', $name)[0];

		// extract issue TeX file
		$filename = $dirname . '/' . $dirname . '.tex';
		$issueTex = $z->getFromName($filename);
		$this->assertNotFalse($issueTex);

		// check issue description
		$this->assertRegExp('/\\\\documentclass\\['. $documentClassOptions .'\\]/', $issueTex);
		if (isset($firstPage)) {
			$this->assertRegExp('/\\\\SetFirstPage\\{' . $firstPage . '\\}/', $issueTex);
		}
		$this->assertRegExp('/\\\\IssueInfo\\{'. $volume .'\\}\\{'. $number .'\\}\\{\\}\\{'. $year .'\\}/', $issueTex);

		// TODO check articles

		$this->assertTrue($z->close());
	}

	/**
	 * Return the DOI of a submission given its ID.
	 *
	 * @param string $submissionId the ID of the submission
	 * @return the DOI or the empty string if no DOI assigned
	 */
	private function getDoi($submissionId) {
		$submissionDao = DAORegistry::getDAO('ArticleDAO'); /* @var submissionDao SubmissionDAO */
		$submission = $submissionDao->getById($submissionId);
		$this->assertNotNull($submission);
		return $submission->getStoredPubId('doi');
	}

	/**
	 * Helper function to accept submission and check DOI assignement.
	 *
	 * @param string $title The submission title
	 * @return the DOI assigned to the submission
	 */
	private function acceptSubmission($title) {
		$this->findSubmissionAsEditor('dbarnes', null, $title);

		// FIXME better retrieval of DOI from submission
		// get submission ID
		$this->waitForPageToLoad();
		$submissionId = array_slice(explode('/', $this->getLocation()), -2, 1)[0];

		// check lack of DOI before accept decision
		$doi = $this->getDoi($submissionId);
		$this->assertEquals($doi, '');

		// accept submission
		$this->recordEditorialDecision('Accept Submission');
		$this->waitForElementPresent('//a[contains(text(), \'Copyediting\')]/*[contains(text(), \'Initiated\')]');

		return $this->getDoi($submissionId);
	}

	/**
	 * Set consent to publish the LaTeX source through the form checkbox.
	 * 
	 * @param $publishTeX bool|null tick or untick the checkbox, toggle if null
	 */
	private function setPublishTeX($publishTeX) {
		$this->waitForElementPresent($selector = 'id=publishTeX');

		$isChecked = $this->isChecked($selector);
		if (is_null($publishTeX) || ($publishTeX && !$isChecked) || (!$publishTeX && $isChecked)) {
			$this->click($selector);
		}
	}

	/**
	 * Open a submission's Metadata modal and set consent to publish the LaTeX source.
	 * 
	 * @param $publishTeX bool|null accept/refuse, toggle if null
	 */
	private function setMetadataPublishTeX($publishTeX) {
		$this->waitForElementPresent($selector = 'link=Metadata');
		$this->click($selector);
		$this->waitJQuery();

		$this->setPublishTeX($publishTeX);

		$this->click('css=button:contains(\'Save\')');

		$this->waitForText('css=.ui-pnotify-text', 'Submission metadata saved.');

		$this->click('css=a.pkpModalCloseButton');
		$this->waitForElementNotPresent('css=div.pkp_modal');
	}

	/**
	 * Change plugin settings: article boilerplate.
	 */
	function testMersenne_settingsArticleBoilerplate() {
		$this->open(self::$baseUrl);

		$this->logIn('admin', 'admin');
		$this->waitForElementPresent($selector = 'link=Workflow');
		$this->clickAndWait($selector);
		$this->waitForElementPresent($selector = 'link=Mersenne');
		$this->click($selector);
		$this->waitForElementPresent("//a[starts-with(@id, 'mersenneForm-uploadFile-articleBoilerplate-button-')]");

		/* add article boilerplate... */
		$this->uploadArticleBoilerplate(dirname(__FILE__) . '/boilerplate.zip');
		$this->waitForElementPresent("//div[@id='articleBoilerplate']/div[@class='pkp_form_file_view']");
		/* ... then delete */
		$this->click('css=[id^=articleBoilerplate-deleteFile-articleBoilerplate-button-]');
		$this->waitForElementPresent('css=div.pkp_modal');
		$this->click('link=OK');
		$this->waitForElementNotPresent('css=div.pkp_modal');
		$this->assertElementNotPresent("//div[@id='articleBoilerplate']/div[@class='pkp_form_file_view']");

		/* add article boilerplate */
		$this->uploadArticleBoilerplate(dirname(__FILE__) . '/boilerplate.zip');
		$this->waitForElementPresent("//div[@id='articleBoilerplate']/div[@class='pkp_form_file_view']");
	}

	/**
	 * Change plugin settings: issue document class options.
	 */
	function testMersenne_settingsDocumentClassOptions() {
		$this->open(self::$baseUrl);

		$this->logIn('admin', 'admin');
		$this->waitForElementPresent($selector = 'link=Workflow');
		$this->clickAndWait($selector);
		$this->waitForElementPresent($selector = 'link=Mersenne');
		$this->click($selector);

		// set issue class options
		$this->waitForElementPresent($selector = 'css=[id^=issueClassOptions-]');
		$this->type($selector, 'TEST');

		$this->waitForElementPresent($selector = 'css=button:contains(\'Save\')');
		$this->click($selector);

		$this->waitForElementPresent("//div[starts-with(@id, 'pkp_notification_') and @class='notifySuccess']");
	}

	/**
	 * @copydoc PKPContentBaseTestCase::_handleStep3()
	 */
	protected function _handleStep3($data) {
		$this->waitForElementPresent($selector = 'id=publishTeX');
		if (isset($data['publishTeX'])) {
			// tick/untick the checkbox
			$this->setPublishTeX($data['publishTeX']);
		}

		parent::_handleStep3($data);
	}

	/**
	 * Test publishTeX checkbox: submission process.
	 */
	function testMersenne_publishTeXNewSubmission() {
		$this->logIn($this->author);

		$publishTeX = FALSE;
		$this->createSubmission(array(
			'title' => 'Mersenne publishTeX test submission (publishTeX=' . ($publishTeX ? 'yes' : 'no') . ')',
			'abstract' => '...',
			'publishTeX' => $publishTeX,
		), 'backend');

		$this->logOut();
	}

	/**
	 * Test publishTeX checkbox: metadata modal.
	 */
	function testMersenne_publishTeXMetadata() {
		$this->findSubmissionAsEditor('dbarnes', null, $this->title);
		$this->waitForPageToLoad();

		// toggle
		$this->setMetadataPublishTeX(null);

		$this->logOut();
	}

	/**
	 * Test DOI creation on submission accept.
	 */
	function testMersenne_doi() {
		$doi1 = $this->acceptSubmission($this->title);
		// check DOI assigned to accepted submission
		$this->assertRegExp('/10.5802\\/publicknowledge.' . $this->doiCounter . '/', $doi1);

		// check with another submission: new DOI, incremented counter
		$this->logOut();
		$doi2 = $this->acceptSubmission('Developing efficacy beliefs in the classroom');
		$this->assertNotEquals($doi1, $doi2);
		$this->assertRegExp('/10.5802\\/publicknowledge.' . ($this->doiCounter + 1) . '/', $doi2);
	}

	/**
	 * Test article helper: article archive preparation.
	 */
	function testMersenne_articleHelper() {
		$this->findSubmissionAsEditor('dbarnes', null, $this->title);
		$this->recordEditorialDecision('Accept Submission');
		$this->waitForElementPresent('//a[contains(text(), \'Copyediting\')]/*[contains(text(), \'Initiated\')]');
		$this->recordEditorialDecision('Send To Production');
		$this->waitForElementPresent('//a[contains(text(), \'Production\')]/*[contains(text(), \'Initiated\')]');

		// upload dummy article source to production ready files
		$sourceFile =  tempnam(sys_get_temp_dir(), 'tst');
		$this->assertTrue(touch($sourceFile));
		$this->waitForElementPresent($selector = 'link=Upload File');
		$this->click($selector);
		$this->uploadWizardFile('LaTeX source', $sourceFile, array ( 'genre' => 'Article Text' ));
		unlink($sourceFile);

		// check Download article button
		$this->waitForElementPresent($selector = '//a[text()=\'Download article\']');

		$tempfile = tempnam(sys_get_temp_dir(), 'tst');
		// download article
		$downloadArticleHref = $this->getAttribute($selector . '/@href');
		$this->download($downloadArticleHref, 'dbarnes', $tempfile);

		$this->validateArticleArchive($tempfile, array(basename($sourceFile)));

		// download article, explicit publishTeX
		$this->setMetadataPublishTeX(true);
		$downloadArticleHref = $this->getAttribute($selector . '/@href');
		$this->download($downloadArticleHref, 'dbarnes', $tempfile);
		$this->validateArticleArchive($tempfile, array(basename($sourceFile)), false);

		// download article, explicit no publishTeX
		$this->setMetadataPublishTeX(false);
		$downloadArticleHref = $this->getAttribute($selector . '/@href');
		$this->download($downloadArticleHref, 'dbarnes', $tempfile);
		$this->validateArticleArchive($tempfile, array(basename($sourceFile)), true);
		
		unlink($tempfile);
	}

	/**
	 * Test article galley: upload article archive as Source galley.
	 */
	function testMersenne_articleGalley() {
		$this->findSubmissionAsEditor('dbarnes', null, $this->title);
		$this->recordEditorialDecision('Accept Submission');
		$this->waitForElementPresent('//a[contains(text(), \'Copyediting\')]/*[contains(text(), \'Initiated\')]');
		$this->recordEditorialDecision('Send To Production');
		$this->waitForElementPresent('//a[contains(text(), \'Production\')]/*[contains(text(), \'Initiated\')]');

		$this->waitForElementPresent($selector = 'link=Add Source galley');
		$this->click($selector);

		// publication-ready file upload dialog
		$this->waitForElementPresent($selector = 'id=genreId');
		$this->select($selector, 'label=Article Text');
		$this->uploadFile(dirname(__FILE__) .'/article.zip');
		$this->waitForElementPresent($selector = 'css=button[id=continueButton]:enabled');
		$this->click($selector);

		$this->waitForElementPresent('css=[id^=name-]');
		$this->click('css=[id=continueButton]');
		$this->waitJQuery();
		$this->waitForElementPresent($selector = 'css=[id=continueButton]');
		$this->click($selector);
		$this->waitJQuery();
		$this->waitForElementNotPresent('css=div.pkp_modal_panel');
	}

	/**
	 * Test the issue helper: issue creation, issue download.
	 */
	function testMersenne_issueHelper() {
		$year = '2018';
		$volume = '1';
		$number = '2';
		$firstPage = '3';

		$this->open(self::$baseUrl);

		// as journal editor...
		$this->logIn('dbarnes');
		$this->waitForElementPresent($selector = 'link=Future Issues');
		$this->clickAndWait($selector);

		$this->waitForElementPresent($selector = 'link=Create Issue');
		$this->click($selector);

		// create new issue
		$this->waitForElementPresent('css=div.pkp_modal');

		$this->waitForElementPresent('css=input[name=firstPage]');
		$this->assertElementNotPresent('css=input[name^=title]');
		$this->assertElementNotPresent('css=fieldset[id=description]');
		$this->assertElementNotPresent('css=fieldset[id=coverImage]');

		$this->type('css=input[name=volume]', $volume);
		$this->type('css=input[name=number]', $number);
		$this->type('css=input[name=year]', $year);
		$this->type('css=input[name=firstPage]', $firstPage);

		$this->click('//button[text()=\'Save\']');
		$this->waitJQuery();
		$this->waitForElementNotPresent('css=div.pkp_modal_panel');

		$this->waitForElementPresent('css=#futureIssuesGridContainer');
		$this->waitForElementPresent('css=a:contains(\'Vol ' . $volume . ' No ' . $number . ' (' . $year . ')\')');

		// TODO add articles

		// download issue
		$this->waitForElementPresent($selector = '//tr[contains(., \'Vol 1 No 2 (2018)\')]//a[text()=\'Download Issue\']');
		$downloadIssueHref = $this->getAttribute($selector . '/@href');
		$tempfile = tempnam(sys_get_temp_dir(), 'tst');
		$this->download($downloadIssueHref, 'dbarnes', $tempfile);

		$this->validateIssueArchive($tempfile, $firstPage, $volume, $number, $year, '');

		unlink($tempfile);
	}
}
