<?php

/**
 * @file tests/classes/MersenneTest.php
 *
 * @class MersenneTest
 * @ingroup plugins_generic_mersenne
 *
 * @brief Test class for MersennePlugin.
 */

import('lib.pkp.tests.PKPTestCase');
import('plugins.generic.mersenne.MersennePlugin');

class MersenneTest extends PKPTestCase {
	/**
	 * @covers MersennePlugin::escapeLaTeX
	 */
	public function testEscapeLaTeX() {
		$actualExpected =  array(
			'a b c' => 'a b c',
			'a & b' => 'a \& b',
			'& % $ # _ { } ~ ^ \ \today' => '\\& \\% \\$ \\# \\_ \\{ \\} \textasciitilde{} \textasciicircum{} \textbackslash{} \textbackslash{}today',
		);
		foreach($actualExpected as $actual => $expected) {
			self::assertEquals($expected, MersennePlugin::escapeLaTeX($actual));
		}
	}
}
?>