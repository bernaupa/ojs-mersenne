<?php

/**
 * @file MersennePlugin.inc.php
 *
 * @class MersennePlugin
 * @ingroup plugins_generic_mersenne
 *
 * @brief Plugin for the Mersenne workflow.
 */

import('lib.pkp.classes.plugins.GenericPlugin');

/**
 * The DOI prefix for any Mersenne DOI.
 */
define('MERSENNE_DOI_PREFIX', '10.5802');

/**
 * The predefined article galley label for article archives.
 */
define('MERSENNE_ARTICLE_SOURCE_GALLEY', 'Source');

class MersennePlugin extends GenericPlugin {

	//
	// Implement methods from Plugin
	//
	/**
	 * @copydoc Plugin::register()
	 */
	function register($category, $path, $mainContextId = NULL) {
		if (parent::register($category, $path, $mainContextId)) {
			if ($this->getEnabled()) {
				// settings tab for plugin
				HookRegistry::register('Templates::Management::Settings::workflow', array($this, 'showWorkflowSettingsTab'));

				// issue metadata and form field: first page
				HookRegistry::register('issuedao::getAdditionalFieldNames', array($this, 'registerIssueFirstPageField'));
				HookRegistry::register('issueform::Constructor', array($this, 'callbackIssueFormFirstPage'));
				HookRegistry::register('issueform::Constructor', array($this, 'overrideFormTemplate'));
				HookRegistry::register('issueform::initdata', array($this, 'callbackIssueFormFirstPage'));
				HookRegistry::register('issueform::readuservars', array($this, 'callbackIssueFormFirstPage'));
				HookRegistry::register('issueform::execute', array($this, 'callbackIssueFormFirstPage'));
				HookRegistry::register('Issue::getProperties::summaryProperties', array($this, 'registerIssueFirstPageProperty'));
				HookRegistry::register('Issue::getProperties::fullProperties', array($this, 'registerIssueFirstPageProperty'));
				HookRegistry::register('Issue::getProperties::values', array($this, 'getIssueFirstPageProperty'));

				// purge fields on Issue Management dialog: pages, pub date...
				HookRegistry::register('issueentrypublicationmetadataform::Constructor', array($this, 'overrideFormTemplate'));

				// hide option to notify all users when publishing a new issue
				HookRegistry::register('assignpublicidentifiersform::Constructor', array($this, 'overrideFormTemplate'));
				
				// register new handlers or replace original handlers
				HookRegistry::register('LoadComponentHandler', array($this, 'loadComponentHandler'));

				// automatic assignment of DOI on acceptation
				HookRegistry::register('EditorAction::recordDecision', array($this, 'assignDOI'));
				// register extra additional fields for DOI
				HookRegistry::register('submissiondao::getAdditionalFieldNames', array($this, 'registerSubmissionDOI'));
				HookRegistry::register('journaldao::getAdditionalFieldNames', array($this, 'registerJournalDOICounter'));

				// custom validation of article archive
				HookRegistry::register('submissionfilesuploadform::validate', array($this, 'checkArticleArchive'));

				// author consent to publish the LaTeX source as submission metadata
				HookRegistry::register('articledao::getAdditionalFieldNames', array($this, 'registerSubmissionPublishTeXField'));
				// submission wizard (step 3)
				HookRegistry::register('submissionsubmitstep3form::Constructor', array($this, 'callbackMetadataFormPublishTeX'));
				HookRegistry::register('submissionsubmitstep3form::initdata', array($this, 'callbackMetadataFormPublishTeX'));
				HookRegistry::register('submissionsubmitstep3form::readuservars', array($this, 'callbackMetadataFormPublishTeX'));
				HookRegistry::register('submissionsubmitstep3form::execute', array($this, 'callbackMetadataFormPublishTeX'));
				// Submission and Publication Metadata modal
				HookRegistry::register('issueentrysubmissionreviewform::Constructor', array($this, 'callbackMetadataFormPublishTeX'));
				HookRegistry::register('issueentrysubmissionreviewform::initdata', array($this, 'callbackMetadataFormPublishTeX'));
				HookRegistry::register('issueentrysubmissionreviewform::readuservars', array($this, 'callbackMetadataFormPublishTeX'));
				HookRegistry::register('issueentrysubmissionreviewform::execute', array($this, 'callbackMetadataFormPublishTeX'));
				// publishTeX field in metadata form
				HookRegistry::register('Templates::Submission::SubmissionMetadataForm::AdditionalMetadata', array($this, 'addSubmissionMetadataPublishTeX'));
				HookRegistry::register('MersennePlugin::exportSubmissionMetadata', array($this, 'exportSubmissionMetadataPublishTeX'));
			}
			return true;
		}
		return false;
	}

	/**
	 * @copydoc Plugin::getDescription()
	 */
	public function getDescription() {
		return __('plugins.generic.mersenne.description');
	}

	/**
	 * @copydoc Plugin::getDisplayName()
	 */
	public function getDisplayName() {
		return __('plugins.generic.mersenne.displayName');
	}

	/**
	 * @copydoc Plugin::getContextSpecificPluginSettingsFile()
	 */
	function getContextSpecificPluginSettingsFile() {
		return $this->getPluginPath() . '/settings.xml';
	}

	/**
	 * @copydoc Plugin::getActions()
	 */
	function getActions($request, $verb) {
		$dispatcher = $request->getDispatcher();
		import('lib.pkp.classes.linkAction.request.RedirectAction');
		return array_merge(
			$this->getEnabled() ?
				array(
					new LinkAction(
						'mersenne',
						new RedirectAction($dispatcher->url(
							$request, ROUTE_PAGE,
							null, 'management', 'settings', 'publication', null, 'mersenne'
						)),
						__('manager.plugins.settings'),
						null
					)
				)
				:
				array(),
			parent::getActions($request, $verb)
		);
	}

	/**
	 * @copydoc PKPPlugin::getTemplatePath
	 */
	function getTemplatePath($inCore = false) {
		return parent::getTemplatePath($inCore) . 'templates/';
	}

	/**
	 * Handler hijacking for the plugin.
	 * @param type $hookName string
	 * @param type $args array
	 * @see PKPComponentRouter::getRpcServiceEndpoint()
	 */
	function loadComponentHandler($hookName, $args) {
		$component =& $args[0];
		switch ($component) {
			// override the ProductionReadyFilesGridHandler (prepareArticle)
			case 'grid.files.productionReady.ProductionReadyFilesGridHandler':
				$component = 'plugins.generic.mersenne.controllers.grid.files.productionReady.MersenneProductionReadyFilesGridHandler';
				return true;

			// override the Future/BackIssueGridHandler (download issue)
			case 'grid.issues.FutureIssueGridHandler':
				$component = 'plugins.generic.mersenne.controllers.grid.issues.MersenneFutureIssueGridHandler';
				return true;
			case 'grid.issues.BackIssueGridHandler':
				$component = 'plugins.generic.mersenne.controllers.grid.issues.MersenneBackIssueGridHandler';
				return true;
			// special grid handler for download issue
			case 'grid.issues.MersenneIssueGridHandler':
				$component = 'plugins.generic.mersenne.controllers.grid.issues.MersenneIssueGridHandler';
				return true;

			// override the ArticleGalleyGridHandler
			case 'grid.articleGalleys.ArticleGalleyGridHandler':
				$component = 'plugins.generic.mersenne.controllers.grid.articleGalleys.MersenneArticleGalleyGridHandler';
				return true;

			// settings handler
			case 'plugins.generic.mersenne.controllers.tab.settings.MersenneSettingsHandler':
				// allow the handlers to get the plugin object
				import($component);
				$className = array_pop(explode('.', $component));
				$className::setPlugin($this);
				return true;
		}
		return false;
	}
	
	/**
	 * Append the Mersenne tab in Workflow settings.
	 * @param $hookName string
	 * @param $args array
	 * @return boolean
	 * @see templates/management/settings/workflow.tpl
	 */
	function showWorkflowSettingsTab($hookName, $args) {
		$output =& $args[2];
		$request = Registry::get('request');
		$dispatcher = $request->getDispatcher();
		$url = $dispatcher->url($request, ROUTE_COMPONENT, null, 'plugins.generic.mersenne.controllers.tab.settings.MersenneSettingsHandler', 'fetch');
		// add a new tab for Mersenne settings
		$output .= '<li><a name="mersenne" href="' . $url . '">' . __('plugins.generic.mersenne.settings.mersenne') . '</a></li>';
		return false;
	}
	
	/**
	 * Use the templates from plugin.
	 * @param string $hookName
	 * @param array $args
	 * @see Form:__construct()
	 */
	function overrideFormTemplate($hookName, $args) {
		$form = $args[0];
		assert(is_a($form, 'Form'));
		$template = $args[1];
		$form->setTemplate($this->getTemplatePath() . $template);
	}

	/**
	 * Add first page as new issue metadata.
	 * @param string $hookName
	 * @param array $args
	 * @see DAO::getAdditionalFieldNames()
	 */
	function registerIssueFirstPageField($hookName, $args) {
		$additionalFields =& $args[1];
		assert(is_array($additionalFields));
		$additionalFields[] = 'firstPage';
	}

	/**
	 * Manage the extra field for first page on issue form.
	 * @param string $hookName
	 * @param array $args
	 */
	function callbackIssueFormFirstPage($hookName, $args) {
		$form = $args[0];
		assert(is_a($form, 'IssueForm'));
		switch($hookName) {
			case 'issueform::Constructor':
				$form->addCheck(
					new FormValidatorCustom(
						$form,
						'firstPage',
						'required',
						'plugins.generic.mersenne.editor.issues.firstPageRequired',
						function ($firstPage) { 
							return ctype_digit($firstPage) && (int)$firstPage > 0;
						}));
				break;
			case 'issueform::initdata':
				$issue = $form->issue;
				assert(is_a($issue, 'Issue'));
				$form->setData('firstPage', $issue->getData('firstPage'));
				break;
			case 'issueform::readuservars':
				$vars =& $args[1];
				assert(is_array($vars));
				$vars[] = 'firstPage';
				break;
			case 'issueform::execute':
				$issue = $args[1];
				assert(is_a($issue, 'Issue'));
				$issue->setdata('firstPage', $form->getData('firstPage'));				
				break;
			default:
				assert(false);
		}
	}

	/**
	 * Add author consent to publish the LaTeX source as submission metadata.
	 * @param $hookName string articledao::getAdditionalFieldNames
	 * @param $args array [
	 *  @option $arg0 ArticleDAO
	 *  @option $additionalFields array list of additional field names to store
	 * ]
	 * @see DAO::getAdditionalFieldNames()
	 */
	function registerSubmissionPublishTeXField($hookName, $args) {
		$additionalFields =& $args[1];
		assert(is_array($additionalFields));
		$additionalFields[] = 'publishTeX';
	}

	/**
	 * Manage in the forms the extra metadata for author consent to publish the LaTeX source.
	 * @param $hookName string Form hook name
	 * @param $args array [
	 *  @option $form SubmissionSubmitStep3Form|IssueEntrySubmissionReviewForm the form
	 *  @option $arg1 mixed
	 * ]
	 */
	function callbackMetadataFormPublishTeX($hookName, $args) {
		$form = $args[0];
		assert(is_a($form, 'SubmissionSubmitStep3Form') || is_a($form, 'IssueEntrySubmissionReviewForm'));
		$submission = is_a($form, 'PKPSubmissionMetadataViewForm') ? $form->getSubmission() : $form->submission;

		switch($hookName) {
			case 'submissionsubmitstep3form::Constructor':
			case 'issueentrysubmissionreviewform::Constructor':
				$form->addCheck(new FormValidatorBoolean($form, 'publishTeX', 'plugins.generic.mersenne.XX'));
				break;
			case 'submissionsubmitstep3form::initdata':
			case 'issueentrysubmissionreviewform::initdata':
				assert(is_a($submission, 'Submission'));
				$form->setData('publishTeX', $submission->hasData('publishTeX') ? $submission->getData('publishTeX') : true);
				break;
			case 'submissionsubmitstep3form::readuservars':
			case 'issueentrysubmissionreviewform::readuservars':
				$vars =& $args[1];
				assert(is_array($vars));
				$vars[] = 'publishTeX';
				break;
			case 'submissionsubmitstep3form::execute':
				// SubmissionSubmitForm::execute() is using an updated version
				// of the submission, not the submission property
				$submission = $args[1];
			case 'issueentrysubmissionreviewform::execute':
				assert(is_a($submission, 'Submission'));
				$submission->setdata('publishTeX', $form->getData('publishTeX'));
				break;
			default:
				assert(false);
		}
	}

	/**
	 * Insert field into submission metadata form for author consent to publish the LaTeX source.
	 * @param $hookName string Templates::Submission::SubmissionMetadataForm::AdditionalMetadata
	 * @param $args array [
	 *  @option $smarty TemplateManager the template engine wrapper
	 *  @option $output string the output
	 * ]
	 * @return boolean false to continue processing
	 * @see lib/pkp/templates/submission/submissionMetadataFormFields.tpl
	 */
	function addSubmissionMetadataPublishTeX($hookName, $args) {
		$smarty =& $args[1];
		$output =& $args[2];

		// hide the description in Metadata modal
		$form = $smarty->getFBV()->getForm(); /* @var $form Form */
		$smarty->assign('showPublishTeXDescription', !is_a($form, 'IssueEntrySubmissionReviewForm'));

		$output .= $smarty->fetch($this->getTemplatePath() . 'submission/submissionMetadataFormPublishTeX.tpl');
		return false;
	}
	
	/**
	 * Forward the opposition of the author for publishing the LaTeX source.
	 * @param $hookName string MersennePlugin::exportSubmissionMetadata
	 * @param $args array [
	 *  @option $submission Submission the submission
	 *  @option $cdrdoidates string the submission metadata as .cdrdoidates file contents
	 * ]
	 */
	function exportSubmissionMetadataPublishTeX($hookName, $args) {
		$submission = $args[0]; /* @var $submission Submission */
		assert(is_a($submission, 'Submission'));
		$cdrdoidates =& $args[1];

		// extend the cdrdoidates with author's choice
		if ($submission->hasData('publishTeX') && !$submission->getData('publishTeX')) {
			// author has explicitly declined to have the LaTeX source published
			$cdrdoidates .= "\ItHasNoTeXPublished\n";
		}
	}

	/**
	 * Add first page to issue properties.
	 * @param $hookName string Issue::getProperties::summaryProperties or Issue::getProperties::fullProperties
	 * @param $args array [
	 *  @option $props array property names
	 *  @option $issue issue
	 *  @option $args array
	 * ]
	 * @see IssueService::getSummaryProperties(), IssueService::getFullProperties()
	 */
	function registerIssueFirstPageProperty($hookName, $args) {
		$props =& $args[0];

		$props[] = 'firstPage';
	}

	/**
	 * Add first page to issue values.
	 * @param type $hookName string Issue::getProperties::values
	 * @param $args array [
	 *  @option $values array property values
	 *  @option $issue issue
	 *  @option $props array property names
	 *  @option $args array
	 * ]
	 * @see IssueService::getProperties()
	 */
	function getIssueFirstPageProperty($hookName, $args) {
		$values =& $args[0];
		$issue =& $args[1];
		$props =& $args[2];

		if (in_array('firstPage', $props)) {
			$values['firstPage'] = (int) $issue->getData('firstPage');
		}
	}

	/**
	 * Create and assign a new Mersenne DOI on accept decision.
	 * @param $hookName string
	 * @param $args array [
	 * 	@option $submission Submission
	 * 	@option $decision array
	 * 	@option $result array
	 * 	@option $recommandation boolean
	 * ]
	 * @see EditorAction::recordDecision()
	 */
	function assignDOI($hookName, $args) {
		$submission =& $args[0];
		$decision =& $args[1];

		// assign only on accept
		if ($decision['decision'] != SUBMISSION_EDITOR_DECISION_ACCEPT) {
			return false;
		}

		if ($submission->getStoredPubId('doi')) {
			// DOI already assigned, skip
			return false;
		}

		$contextDao = Application::getContextDAO();
		$context = $contextDao->getById($submission->getContextId());

		$doiNext = $context->getSetting('doiCounter');
		if (is_null($doiNext)) {
			// DOI counter not initialized, skip
			return false;
		}
		// bump counter for next DOI
		$context->updateSetting('doiCounter', $doiNext + 1);

		// format Mersenne DOI
		$acro = $context->getLocalizedAcronym();
		$doi = MERSENNE_DOI_PREFIX . "/" . strtolower($acro) . "." . (string)$doiNext;

		// @see PubIdPlugin::manage()
		$submission->getDAO()->changePubId($submission->getId(), 'doi', $doi);
		$submission->setStoredPubId('doi', $doi);

		return false;
	}

	/**
	 * Add the DOI to the submission.
	 * @param $hookName string
	 * @param $params array
	 * @see DAO::getAdditionalFieldNames()
	 */
	function registerSubmissionDOI($hookName, $params) {
		$fields =& $params[1];
		$fields[] = 'pub-id::doi';
	}

	/**
	 * Add the counter to the journal.
	 * @param $hookName string
	 * @param $params array
	 * @see DAO:getAdditionalFieldNames()
	 */
	function registerJournalDOICounter($hookName, $params) {
		$fields =& $params[1];
		$fields[] = 'doiCounter';
	}

	/**
	 * Validate the structure of an archive uploaded as article archive.
	 * @param string $hookName
	 * @param array $args
	 * @return boolean
	 * @see Form::validate()
	 */
	function checkArticleArchive($hookName, $args) {
		$form = $args[0];
		assert(is_a($form, 'SubmissionFilesUploadForm'));

		if ($form->getData('fileStage') != SUBMISSION_FILE_PROOF) {
			// not an article galley
			return FALSE;
		}
		assert($form->getData('assocType') === ASSOC_TYPE_REPRESENTATION);
		$articleGalleyDao = DAORegistry::getDAO('ArticleGalleyDAO');
		$articleGalley = $articleGalleyDao->getById($form->getData('assocId'));
		assert($articleGalley);
		if ($articleGalley->getLabel() !== MERSENNE_ARTICLE_SOURCE_GALLEY) {
			// not a article archive galley
			return FALSE;
		}

		// the POSTed archive file
		$uploadedFilePath = $_FILES['uploadedFile']['tmp_name'];
		$z = new ZipArchive();
		$res = $z->open($uploadedFilePath, ZipArchive::CHECKCONS);
		if ($res !== TRUE) {
			$form->addError('uploadedFile', __('plugins.generic.mersenne.archive.zipOpenFail'));
			return FALSE;
		}
		$basename = '';
		$hasTex = $hasCdrdoidates = FALSE;
		// check the structure of the archive
		for ($i = 0; $i < $z->numFiles; $i++) {
			$name = $z->getNameIndex($i);
			if (!$name) {
				$form->addError('uploadedFile', __('plugins.generic.mersenne.archive.zipReadFail: {$status}.', array('status' => $z->getStatusString())));
				break;
			}

			// TODO use $z->statIndex($i)? https://stackoverflow.com/questions/9905279/retrieve-folders-names-only-from-a-zip-file-with-php/9905810#9905810
			$tokens = explode('/', $name);
			if (empty($basename)) {
				// keep directory name as article identifier
				$basename = $tokens[0];
			}

			if ($tokens[0] != $basename) {
				$form->addError('uploadedFile', __('plugins.generic.mersenne.archive.unexpectedFile', array('filename' => $name)));
				break;
			}

			$hasTex         |= ($tokens[1] === $basename.'.tex');
			$hasCdrdoidates |= ($tokens[1] === $basename.'.cdrdoidates');
		}
		$z->close();

		if (!$hasTex) {
			$form->addError('uploadedFile', __('plugins.generic.mersenne.archive.texFileNotFound'));
		}
		if (!$hasCdrdoidates) {
			$form->addError('uploadedFile', __('plugins.generic.mersenne.archive.cdrdoidatesFileNotFound'));
		}

		return FALSE;
	}

	/**
	 * Replace LaTeX special characters in a string.
	 * @param $string string
	 * @return string the escaped string
	 * @see https://tex.stackexchange.com/questions/34580/escape-character-in-latex/34586#34586
	 */
	static function escapeLaTeX($string) {
		$characterToEscaped = array(
			'&'  => '\&',
			'%'  => '\%',
			'$'  => '\$',
			'#'  => '\#',
			'_'  => '\_',
			'{'  => '\{',
			'}'  => '\}',
			'~'  => '\textasciitilde{}',
			'^'  => '\textasciicircum{}',
			'\\' => '\textbackslash{}',
		);
		return preg_replace_callback(
			"/([". implode(array_keys($characterToEscaped), '\\') . "])/",
			function ($matches) use ($characterToEscaped) { return $characterToEscaped[$matches[0]]; },
			$string);
	}
}

?>
